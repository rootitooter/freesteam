extern crate cc;

fn main() {
    cc::Build::new()
        .cpp(true)
        .include("clib/incl")
        .file("clib/src/lib.cpp")
        .file("clib/src/flat.cpp")
        .extra_warnings(false)
        .compile("libfscpp.a");
    
}