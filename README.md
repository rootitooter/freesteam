# freesteam
Freesteam is a completely FOSS reimplementation of the proprietary Steamworks API
written in Rust and C++. It can be used to run Steam games without having
Steam running, or on a system where Steam is no longer supported.

It's aimed at Linux, but it should work on other operating systems.

## Usage
Compile using `cargo`:

```sh
cargo build --release
```

Grab `target/release/libsteam_api.so` and replace the libsteam_api.so file in
the application you are trying to run.

## License (LGPLv3)
Anyone is free to contribute to this project.

See LICENSE for full license.
