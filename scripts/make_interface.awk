match($0, /class I(\w+)/, a) {
    printf("class C%s : public I%s\n", a[1], a[1]);
    next;
}

/\t[a-zA-Z0-9_]+\(/ {
    print;
    next;
}

/\tbool .+/ {
    print substr($0, 1, length($0)-2) " { return false; }"
    next;
}

/\tvoid .+/ {
    print substr($0, 1, length($0)-2) " { return; }"
    next;
}

/\tSteamAPICall_t .+/ {
    print substr($0, 1, length($0)-2) " { return k_uAPICallInvalid; }"
    next;
}

/\tCSteamID .+/ {
    print substr($0, 1, length($0)-2) " { return CSteamID(); }"
    next;
}

/\tEResult .+/ {
    print substr($0, 1, length($0)-2) " { return k_EResultNoConnection; }"
    next;
}

/^\t[a-zA-Z]+.*/ {
    # Unmatched stuff
    print substr($0, 1, length($0)-2) " { return 0; }";
    next;
}

{ print; }