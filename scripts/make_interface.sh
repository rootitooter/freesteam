#!/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "#pragma once"
echo '#include "Steam.h"'
echo '#include "steam/steam_api.h"'
echo '#include "'$1'"'
echo

# Get just the class
awk 'BEGIN { p=0; }
/class .*/ { p = 1 }
p { print; }
/};.*/ { p = 0; }' $1 |
    sed 's/virtual //;
         s/ = 0;/;/;
         s/\; \/\/.*/\; /' |
    awk -f $DIR/make_interface.awk