mod c_api {
    use std::{env, fs};

    #[no_mangle]
    pub extern fn get_appid() -> i32 {
        // Will look at SteamAppId environment variable.
        // If not available, look at steam_appid.txt.
        // If any of these isn't valid, return 480

        env::var_os("SteamAppId")
            .and_then(|v| v.into_string().ok())
            .and_then(|v| v.parse::<i32>().ok())
            .or(fs::read_to_string("steam_appid.txt").ok()
                    .and_then(|v| v.trim().parse::<i32>().ok()))
            .unwrap_or(480)
    }
}

#[cfg(test)]
mod tests;
