use galvanic_test::test_suite;

test_suite! {
    name fs_utils_test;

    use crate::c_api;
    use std::env;
    use std::fs::{File, remove_file};
    use std::io::Write;

    fixture env_var(key: String, val: String) -> () {
        setup(&mut self) {
            env::set_var(&self.key, &self.val);
        }

        tear_down(&self) {
            env::remove_var(&self.key);
        }
    }

    fixture steamappid_file(appid: i32) -> File {
        setup(&mut self) {
            let mut file = File::create("steam_appid.txt").unwrap();
            file.write_all(self.appid.to_string().as_bytes()).unwrap();

            file
        }

        tear_down(&self) {
            remove_file("steam_appid.txt").unwrap();
        }
    }

    test appid_env(env_var(String::from("SteamAppId"), String::from("320"))) {
        assert_eq!(c_api::get_appid(), 320);
    }

    test appid_noenv() {
        assert_eq!(c_api::get_appid(), 480);
    }

    test appid_file(steamappid_file(320)) {
        assert_eq!(c_api::get_appid(), 320);
    }
}
