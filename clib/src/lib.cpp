#include "Steam.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include "steam/isteamclient.h"
#include "steam/steamclientpublic.h"
#include "steam/steamtypes.h"
#include "fs_utils.h"

S_API bool S_CALLTYPE SteamAPI_Init() {
    printf("[S_API] Called SteamAPI_Init\n");
    // All this function does is essentially initialize SteamClient_Client.
    // If it's already set, it means it's already initialized, so why bother
    // doing anything?
    if (SteamClient_Client != NULL)
        return true;

    // Now, we want to do some initialization of our own!
    USERID = new CSteamID(1, k_EUniversePublic, k_EAccountTypeIndividual);

    APPID = get_appid();

    SteamClient_Client = (ISteamClient*)SteamInternal_CreateInterface(STEAMCLIENT_INTERFACE_VERSION);
    if(SteamClient_Client == NULL) {
        fprintf(stderr, "[S_API] SteamAPI_Init failed: Failed to create interface\n");
        return false;
    }
    ContextCounter++;

    return true;
}

S_API bool S_CALLTYPE SteamAPI_InitSafe() {
    printf("[S_API] Called SteamAPI_InitSafe\n");
    return SteamAPI_Init();
}

S_API void S_CALLTYPE SteamAPI_Shutdown() {
    printf("[S_API] Called SteamAPI_Shutdown\n");
    return;
}

S_API bool S_CALLTYPE SteamAPI_RestartAppIfNecessary(uint32 AppID) {
    // Normally, this function would restart the app using steam if steam's not
    // running. We want none of that. Just return false.
    // Even though IsSteamRunning is always going to return true, this will
    // still return false if it doesn't detect a valid appID, so we need to
    // provide one somehow. Best way is to through the steam_appid.txt file.
    printf("[S_API] Called SteamAPI_RestartAppIfNecessary\n");
    return false;
}

S_API bool S_CALLTYPE SteamAPI_IsSteamRunning() {
    printf("[S_API] Called SteamAPI_IsSteamRunning\n");
    // This needs to return true, otherwise, SteamAPI_Init will fail.
    return true;
}

S_API const char *SteamAPI_GetSteamInstallPath() {
    printf("[S_API] Called SteamAPI_GetSteamInstallPath\n");
    return getSteamInstallpath();
}

S_API HSteamUser SteamAPI_GetHSteamUser() {
    printf("[S_API] Called GetHSteamUser\n");
    return (HSteamUser)1;
}

S_API HSteamPipe SteamAPI_GetHSteamPipe() {
    printf("[S_API] Called GetHSteamPipe\n");
    return (HSteamPipe)1;
}

S_API void SteamAPI_UnregisterCallback(CCallbackBase *pCallback) { return; }
S_API void SteamAPI_RegisterCallback(CCallbackBase *pCallback, int iCallback) { return; }

S_API void * S_CALLTYPE SteamInternal_CreateInterface(const char *ver) {
    // This function is responsible for getting all sorts of Steam Interfaces,
    // like ISteamClient and ISteamUser. It'll typically call onto the Steam
    // client to request an interface, but that isn't something we can do here,
    // so instead we'll create our own interfaces and serve them.

    printf("[S_API] Called SteamInternal_CreateInterface %s\n", ver);

    if(strcmp(ver, STEAMCLIENT_INTERFACE_VERSION) == 0)
        return (ISteamClient*)new CSteamClient();
    
    return NULL;
}

S_API void* S_CALLTYPE SteamInternal_ContextInit(void *pContextInitData) {
    printf("[S_API] Called SteamInternal_ContextInit\n");

    if (pContextInitData != NULL) {
        ContextInit *initData = (ContextInit*)pContextInitData;

        char *pPointer = reinterpret_cast<char*>(pContextInitData);

        if (initData->Counter != ContextCounter) {
            // sizeof(void*) is a good way to get the architecture.
            // It returns, in bytes, the size of an address.
            // 4 bytes = 32 bits, 8 bytes = 64 bits
            initData->pFn(pPointer + sizeof(void*)*2);
            initData->Counter = ContextCounter;
        }
        return pPointer + sizeof(void*)*2;
    }

    return NULL;
}

// Helper functions!
const char *determineLang() {
    if (LANG != NULL)
        return LANG;
    
    LANG = getenv("FS_LANG");
    if (LANG != NULL || strcmp("LANG", "") != 0)
        return LANG;
    
    LANG = setlocale(LC_CTYPE, NULL);

    if(!strncmp(LANG, "ar", 2))
        LANG = "arabic";
    else if(!strncmp(LANG, "bg", 2))
        LANG = "bulgarian";
    else if(!strncmp(LANG, "zh_CN", 5))
        LANG = "schinese";
    else if(!strncmp(LANG, "zh_TW", 5))
        LANG = "tchinese";
    else if(!strncmp(LANG, "cs", 2))
        LANG = "czech";
    else if(!strncmp(LANG, "da", 2))
        LANG = "danish";
    else if(!strncmp(LANG, "nl", 2))
        LANG = "dutch";
    else if(!strncmp(LANG, "fi", 2))
        LANG = "finnish";
    else if(!strncmp(LANG, "fr", 2))
        LANG = "french";
    else if(!strncmp(LANG, "de", 2))
        LANG = "german";
    else if(!strncmp(LANG, "el", 2))
        LANG = "greek";
    else if(!strncmp(LANG, "hu", 2))
        LANG = "hungarian";
    else if(!strncmp(LANG, "it", 2))
        LANG = "italian";
    else if(!strncmp(LANG, "ja", 2))
        LANG = "japanese";
    else if(!strncmp(LANG, "ko", 2))
        LANG = "koreana";
    else if(!strncmp(LANG, "no", 2))
        LANG = "norwegian";
    else if(!strncmp(LANG, "pl", 2))
        LANG = "polish";
    else if(!strncmp(LANG, "pt_BR", 5))
        LANG = "brazilian";
    else if(!strncmp(LANG, "pt", 2))
        LANG = "portuguese";
    else if(!strncmp(LANG, "ro", 2))
        LANG = "romanian";
    else if(!strncmp(LANG, "ru", 2))
        LANG = "russian";
    else if(!strncmp(LANG, "es", 2))
        LANG = "spanish";
    else if(!strncmp(LANG, "es_419", 5))
        LANG = "latam";
    else if(!strncmp(LANG, "sv", 2))
        LANG = "swedish";
    else if(!strncmp(LANG, "th", 2))
        LANG = "thai";
    else if(!strncmp(LANG, "tr", 2))
        LANG = "turkish";
    else if(!strncmp(LANG, "uk", 2))
        LANG = "ukranian";
    else if(!strncmp(LANG, "vn", 2))
        LANG = "vietnamese";
    else
        LANG = "english";
   
    return LANG;
}

const char *getSteamInstallpath() {
    if (INSTALLPATH == NULL) {
        INSTALLPATH = getenv("FS_STEAMINSTALLPATH");

        if (INSTALLPATH == NULL || strcmp(INSTALLPATH, "") == 0) {
            #ifdef _WIN32
                INSTALLPATH = strcat(getenv("APPDATA"), "\\freesteam\\freesteam");
            #elif defined(__APPLE__)
                INSTALLPATH = strcat(getenv("HOME"), "/Library/Application Support/freesteam/");
            #else
                char *xdg_home;
                xdg_home = getenv("XDG_DATA_HOME");
                if (xdg_home == NULL || strcmp(xdg_home, ""))
                    xdg_home = strcat(getenv("HOME"), "/.local/share");
                // TODO: Use getpwdid to get home directory if HOME environment variable not set
                INSTALLPATH = strcat(xdg_home, "/freesteam");
            #endif
        }
    }

    return INSTALLPATH;
}

const char *getUsername() {
    char *username;
    username = getenv("USER");
    if (username == NULL || !strcmp(username, ""))
        username = "freesteam";

    return username;
}
