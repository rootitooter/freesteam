#pragma once
#include "Steam.h"
#include "steam/steam_api.h"
#include "steam/isteamapps.h"
#ifdef _WIN32
#include <direct.h>
#else
#include <unistd.h>
#endif
#include <stdlib.h>

class CSteamApps : public ISteamApps {
public:
    bool BIsSubscribed() { return true; }
	bool BIsLowViolence() { return false; }
	bool BIsCybercafe() { return false; }
	bool BIsVACBanned() { return false; }
	const char *GetCurrentGameLanguage() { return determineLang(); }
    // TODO
	const char *GetAvailableGameLanguages() { return determineLang(); }

	// only use this member if you need to check ownership of another game related to yours, a demo for example
	bool BIsSubscribedApp( AppId_t appID ) { return true; }

	// Takes AppID of DLC and checks if the user owns the DLC & if the DLC is installed
	bool BIsDlcInstalled( AppId_t appID ) { return true; }

	// returns the Unix time of the purchase of the app
	uint32 GetEarliestPurchaseUnixTime( AppId_t nAppID ) { return 0; }

	// Checks if the user is subscribed to the current app through a free weekend
	// This function will return false for users who have a retail or other type of license
	// Before using, please ask your Valve technical contact how to package and secure your free weekened
	bool BIsSubscribedFromFreeWeekend() { return false; }

	// Returns the number of DLC pieces for the running app
    // TODO: Load DLC from file!
	int GetDLCCount() { return 0; }

	// Returns metadata for DLC by index, of range [0, GetDLCCount()]
	bool BGetDLCDataByIndex( int iDLC, AppId_t *pAppID, bool *pbAvailable, char *pchName, int cchNameBufferSize ) {
        return false;
    }

	// Install/Uninstall control for optional DLC
	void InstallDLC( AppId_t nAppID ) { return; }
	void UninstallDLC( AppId_t nAppID ) { return; }
	
	// Request legacy cd-key for yourself or owned DLC. If you are interested in this
	// data then make sure you provide us with a list of valid keys to be distributed
	// to users when they purchase the game, before the game ships.
	// You'll receive an AppProofOfPurchaseKeyResponse_t callback when
	// the key is available (which may be immediately).
	void RequestAppProofOfPurchaseKey( AppId_t nAppID ) { return; }

	bool GetCurrentBetaName( char *pchName, int cchNameBufferSize ) {
        pchName = "public";
        return true;
    } // returns current beta branch name, 'public' is the default branch
	bool MarkContentCorrupt( bool bMissingFilesOnly ) { return false; }// signal Steam that game files seems corrupt or missing
	uint32 GetInstalledDepots( AppId_t appID, DepotId_t *pvecDepots, uint32 cMaxDepots ) {
        return 0;
    } // return installed depots in mount order

	// returns current app install folder for AppID, returns folder name length
	uint32 GetAppInstallDir( AppId_t appID, char *pchFolder, uint32 cchFolderBufferSize ) {
        pchFolder = getcwd(pchFolder, cchFolderBufferSize);
        return strlen(pchFolder);
    }
	bool BIsAppInstalled( AppId_t appID ) { return true; } // returns true if that app is installed (not necessarily owned)
	
	CSteamID GetAppOwner() { return *USERID; } // returns the SteamID of the original owner. If different from current user, it's borrowed

	// Returns the associated launch param if the game is run via steam://run/<appid>//?param1=value1;param2=value2;param3=value3 etc.
	// Parameter names starting with the character '@' are reserved for internal use and will always return and empty string.
	// Parameter names starting with an underscore '_' are reserved for steam features -- they can be queried by the game,
	// but it is advised that you not param names beginning with an underscore for your own features.
	const char *GetLaunchQueryParam( const char *pchKey ) {
        // Just return an environment variable :)
        return getenv(pchKey);
    }

	// get download progress for optional DLC
	bool GetDlcDownloadProgress( AppId_t nAppID, uint64 *punBytesDownloaded, uint64 *punBytesTotal ) {
        *punBytesDownloaded = *punBytesTotal;
        return true;
    }

	// return the buildid of this app, may change at any time based on backend updates to the game
	int GetAppBuildId() { return 0; } // TODO: Add environment var

	// Request all proof of purchase keys for the calling appid and asociated DLC.
	// A series of AppProofOfPurchaseKeyResponse_t callbacks will be sent with
	// appropriate appid values, ending with a final callback where the m_nAppId
	// member is k_uAppIdInvalid (zero).
	void RequestAllProofOfPurchaseKeys() { return; }

	CALL_RESULT( FileDetailsResult_t )
	SteamAPICall_t GetFileDetails( const char* pszFileName ) {
        return k_ESteamAPICallFailureNetworkFailure;
    }
};