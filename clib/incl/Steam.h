#pragma once
#define STEAM_API_EXPORTS
#include "steam/steam_api.h"
#include "steam/steamtypes.h"

const char *LANG;
const char *USER;
const char *INSTALLPATH;
const char *GAME_INSTALLPATH;

uint32 APPID;
CSteamID *USERID;
uintp ContextCounter = 0;
ISteamClient *SteamClient_Client;

struct ContextInit {
    void (*pFn)(void* pCtx);
    uintp Counter;
    CSteamAPIContext ctx;
};

const char *determineLang();
const char *getSteamInstallpath();
const char *getUsername();

#include "SteamClient.h"
#include "SteamUser.h"
#include "SteamAppTicket.h"
#include "SteamInternal.h"
#include "SteamUtils.h"
#include "SteamApps.h"
#include "SteamUserStats.h"
#include "SteamFriends.h"
