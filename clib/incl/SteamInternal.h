#pragma once
#include "steam/steam_api.h"
#include "steam/steam_api_internal.h"

inline void S_CALLTYPE SteamInternal_OnContextInit( void* p )
{
	((CSteamAPIContext*)p)->Clear();
	if ( SteamAPI_GetHSteamPipe() )
		((CSteamAPIContext*)p)->Init();
}
inline CSteamAPIContext& SteamInternal_ModuleContext()
{
	// SteamInternal_ContextInit takes a base pointer for the equivalent of
	// struct { void (*pFn)(void* pCtx); uintp counter; CSteamAPIContext ctx; }
	// Do not change layout of 2 + sizeof... or add non-pointer aligned data!
	// NOTE: declaring "static CSteamAPIConext" creates a large function
	// which queries the initialization status of the object. We know that
	// it is pointer-aligned and fully memset with zeros, so just alias a
	// static buffer of the appropriate size and call it a CSteamAPIContext.
	static void* s_CallbackCounterAndContext[ 2 + sizeof(CSteamAPIContext)/sizeof(void*) ] = { (void*)&SteamInternal_OnContextInit, 0 };
	return *(CSteamAPIContext*)SteamInternal_ContextInit( s_CallbackCounterAndContext );
}

S_API ISteamClient *SteamClient() {
    printf("[S_API] Called SteamClient\n");
    return SteamInternal_ModuleContext().SteamClient();
}
S_API ISteamUser *SteamUser() {
    printf("[S_API] Called SteamUser\n");
    return SteamInternal_ModuleContext().SteamUser();
}
S_API ISteamFriends *SteamFriends() {
    printf("[S_API] Called SteamFriends\n");
    return SteamInternal_ModuleContext().SteamFriends();
}
S_API ISteamUtils *SteamUtils() {
    printf("[S_API] Called SteamUtils\n");
    return SteamInternal_ModuleContext().SteamUtils();
}
S_API ISteamMatchmaking *SteamMatchmaking() {
    printf("[S_API] Called SteamMatchmaking\n");
    return SteamInternal_ModuleContext().SteamMatchmaking();
}
S_API ISteamUserStats *SteamUserStats() {
    printf("[S_API] Called SteamUserStats\n");
    return SteamInternal_ModuleContext().SteamUserStats();
}
S_API ISteamApps *SteamApps() {
    printf("[S_API] Called SteamApps\n");
    return SteamInternal_ModuleContext().SteamApps();
}
S_API ISteamMatchmakingServers *SteamMatchmakingServers() {
    printf("[S_API] Called SteamMatchmakingServers\n");
    return SteamInternal_ModuleContext().SteamMatchmakingServers();
}
S_API ISteamNetworking *SteamNetworking() {
    printf("[S_API] Called SteamNetworking\n");
    return SteamInternal_ModuleContext().SteamNetworking();
}
S_API ISteamRemoteStorage *SteamRemoteStorage() {
    printf("[S_API] Called SteamRemoteStorage\n");
    return SteamInternal_ModuleContext().SteamRemoteStorage();
}
S_API ISteamScreenshots *SteamScreenshots() {
    printf("[S_API] Called SteamScreenshots\n");
    return SteamInternal_ModuleContext().SteamScreenshots();
}
S_API ISteamHTTP *SteamHTTP() {
    printf("[S_API] Called SteamHTTP\n");
    return SteamInternal_ModuleContext().SteamHTTP();
}
S_API ISteamController *SteamController() {
    printf("[S_API] Called SteamController\n");
    return SteamInternal_ModuleContext().SteamController();
}
S_API ISteamUGC *SteamUGC() {
    printf("[S_API] Called SteamUGC\n");
    return SteamInternal_ModuleContext().SteamUGC();
}
S_API ISteamAppList *SteamAppList() {
    printf("[S_API] Called SteamAppList\n");
    return SteamInternal_ModuleContext().SteamAppList();
}
S_API ISteamMusic *SteamMusic() {
    printf("[S_API] Called SteamMusic\n");
    return SteamInternal_ModuleContext().SteamMusic();
}
S_API ISteamMusicRemote *SteamMusicRemote() {
    printf("[S_API] Called SteamMusicRemote\n");
    return SteamInternal_ModuleContext().SteamMusicRemote();
}
S_API ISteamHTMLSurface *SteamHTMLSurface() {
    printf("[S_API] Called SteamHTMLSurface\n");
    return SteamInternal_ModuleContext().SteamHTMLSurface();
}
S_API ISteamInventory *SteamInventory() {
    printf("[S_API] Called SteamInventory\n");
    return SteamInternal_ModuleContext().SteamInventory();
}
S_API ISteamVideo *SteamVideo() {
    printf("[S_API] Called SteamVideo\n");
    return SteamInternal_ModuleContext().SteamVideo();
}
S_API ISteamParentalSettings *SteamParentalSettings() {
    printf("[S_API] Called\n");
    return SteamInternal_ModuleContext().SteamParentalSettings();
}
