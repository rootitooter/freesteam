#pragma once
#include "Steam.h"
#include "steam/isteamappticket.h"

struct AppOwnershipTicket {
    AppId_t appId;
    CSteamID steamId;
    char *signature;
};

class CSteamAppTicket : public ISteamAppTicket {
public:
    uint32 GetAppOwnershipTicketData(uint32 nAppID, void *pvBuffer,
                uint32 cbBufferLength, uint32 *piAppId, uint32 *piSteamId,
                uint32 *piSignature, uint32 *pcbSignature ) {
        printf("Called GetAppOwnershipData\n");

        *piAppId = 0;
        *((AppId_t*)((char*)pvBuffer + *piAppId)) = nAppID;

        *piSteamId = sizeof(AppId_t);
        *((CSteamID*)((char*)pvBuffer + *piSteamId)) = *USERID;

        // TODO: Generate a signature.
        *piSignature = *piSteamId + sizeof(CSteamID);
        *pcbSignature = 0;

		return 0;
    };
};