#pragma once
#include "Steam.h"
#include "steam/steam_api.h"
#include "steam/isteamugc.h"

class CSteamUGC : public ISteamUGC
{
public:

	// Query UGC associated with a user. Creator app id or consumer app id must be valid and be set to the current running app. unPage should start at 1.
	UGCQueryHandle_t CreateQueryUserUGCRequest( AccountID_t unAccountID, EUserUGCList eListType, EUGCMatchingUGCType eMatchingUGCType, EUserUGCListSortOrder eSortOrder, AppId_t nCreatorAppID, AppId_t nConsumerAppID, uint32 unPage ) { return 0; }

	// Query for all matching UGC. Creator app id or consumer app id must be valid and be set to the current running app. unPage should start at 1.
	UGCQueryHandle_t CreateQueryAllUGCRequest( EUGCQuery eQueryType, EUGCMatchingUGCType eMatchingeMatchingUGCTypeFileType, AppId_t nCreatorAppID, AppId_t nConsumerAppID, uint32 unPage ) { return 0; }

	// Query for the details of the given published file ids (the RequestUGCDetails call is deprecated and replaced with this)
	UGCQueryHandle_t CreateQueryUGCDetailsRequest( PublishedFileId_t *pvecPublishedFileID, uint32 unNumPublishedFileIDs ) { return 0; }

	// Send the query to Steam
	CALL_RESULT( SteamUGCQueryCompleted_t )
	SteamAPICall_t SendQueryUGCRequest( UGCQueryHandle_t handle ) { return k_uAPICallInvalid; }

	// Retrieve an individual result after receiving the callback for querying UGC
	bool GetQueryUGCResult( UGCQueryHandle_t handle, uint32 index, SteamUGCDetails_t *pDetails ) { return false; }
	bool GetQueryUGCPreviewURL( UGCQueryHandle_t handle, uint32 index, OUT_STRING_COUNT(cchURLSize) char *pchURL, uint32 cchURLSize ) { return false; }
	bool GetQueryUGCMetadata( UGCQueryHandle_t handle, uint32 index, OUT_STRING_COUNT(cchMetadatasize) char *pchMetadata, uint32 cchMetadatasize ) { return false; }
	bool GetQueryUGCChildren( UGCQueryHandle_t handle, uint32 index, PublishedFileId_t* pvecPublishedFileID, uint32 cMaxEntries ) { return false; }
	bool GetQueryUGCStatistic( UGCQueryHandle_t handle, uint32 index, EItemStatistic eStatType, uint64 *pStatValue ) { return false; }
	uint32 GetQueryUGCNumAdditionalPreviews( UGCQueryHandle_t handle, uint32 index ) { return 0; }
	bool GetQueryUGCAdditionalPreview( UGCQueryHandle_t handle, uint32 index, uint32 previewIndex, OUT_STRING_COUNT(cchURLSize) char *pchURLOrVideoID, uint32 cchURLSize, OUT_STRING_COUNT(cchURLSize) char *pchOriginalFileName, uint32 cchOriginalFileNameSize, EItemPreviewType *pPreviewType ) { return false; }
	uint32 GetQueryUGCNumKeyValueTags( UGCQueryHandle_t handle, uint32 index ) { return 0; }
	bool GetQueryUGCKeyValueTag( UGCQueryHandle_t handle, uint32 index, uint32 keyValueTagIndex, OUT_STRING_COUNT(cchKeySize) char *pchKey, uint32 cchKeySize, OUT_STRING_COUNT(cchValueSize) char *pchValue, uint32 cchValueSize ) { return false; }

	// Release the request to free up memory, after retrieving results
	bool ReleaseQueryUGCRequest( UGCQueryHandle_t handle ) { return false; }

	// Options to set for querying UGC
	bool AddRequiredTag( UGCQueryHandle_t handle, const char *pTagName ) { return false; }
	bool AddExcludedTag( UGCQueryHandle_t handle, const char *pTagName ) { return false; }
	bool SetReturnOnlyIDs( UGCQueryHandle_t handle, bool bReturnOnlyIDs ) { return false; }
	bool SetReturnKeyValueTags( UGCQueryHandle_t handle, bool bReturnKeyValueTags ) { return false; }
	bool SetReturnLongDescription( UGCQueryHandle_t handle, bool bReturnLongDescription ) { return false; }
	bool SetReturnMetadata( UGCQueryHandle_t handle, bool bReturnMetadata ) { return false; }
	bool SetReturnChildren( UGCQueryHandle_t handle, bool bReturnChildren ) { return false; }
	bool SetReturnAdditionalPreviews( UGCQueryHandle_t handle, bool bReturnAdditionalPreviews ) { return false; }
	bool SetReturnTotalOnly( UGCQueryHandle_t handle, bool bReturnTotalOnly ) { return false; }
	bool SetReturnPlaytimeStats( UGCQueryHandle_t handle, uint32 unDays ) { return false; }
	bool SetLanguage( UGCQueryHandle_t handle, const char *pchLanguage ) { return false; }
	bool SetAllowCachedResponse( UGCQueryHandle_t handle, uint32 unMaxAgeSeconds ) { return false; }

	// Options only for querying user UGC
	bool SetCloudFileNameFilter( UGCQueryHandle_t handle, const char *pMatchCloudFileName ) { return false; }

	// Options only for querying all UGC
	bool SetMatchAnyTag( UGCQueryHandle_t handle, bool bMatchAnyTag ) { return false; }
	bool SetSearchText( UGCQueryHandle_t handle, const char *pSearchText ) { return false; }
	bool SetRankedByTrendDays( UGCQueryHandle_t handle, uint32 unDays ) { return false; }
	bool AddRequiredKeyValueTag( UGCQueryHandle_t handle, const char *pKey, const char *pValue ) { return false; }

	// DEPRECATED - Use CreateQueryUGCDetailsRequest call above instead!
	SteamAPICall_t RequestUGCDetails( PublishedFileId_t nPublishedFileID, uint32 unMaxAgeSeconds ) { return k_uAPICallInvalid; }

	// Steam Workshop Creator API
	CALL_RESULT( CreateItemResult_t )
	SteamAPICall_t CreateItem( AppId_t nConsumerAppId, EWorkshopFileType eFileType ) { return k_uAPICallInvalid; }

	UGCUpdateHandle_t StartItemUpdate( AppId_t nConsumerAppId, PublishedFileId_t nPublishedFileID ) { return 0; }

	bool SetItemTitle( UGCUpdateHandle_t handle, const char *pchTitle ) { return false; }
	bool SetItemDescription( UGCUpdateHandle_t handle, const char *pchDescription ) { return false; }
	bool SetItemUpdateLanguage( UGCUpdateHandle_t handle, const char *pchLanguage ) { return false; }
	bool SetItemMetadata( UGCUpdateHandle_t handle, const char *pchMetaData ) { return false; }
	bool SetItemVisibility( UGCUpdateHandle_t handle, ERemoteStoragePublishedFileVisibility eVisibility ) { return false; }
	bool SetItemTags( UGCUpdateHandle_t updateHandle, const SteamParamStringArray_t *pTags ) { return false; }
	bool SetItemContent( UGCUpdateHandle_t handle, const char *pszContentFolder ) { return false; }
	bool SetItemPreview( UGCUpdateHandle_t handle, const char *pszPreviewFile ) { return false; }
	bool RemoveItemKeyValueTags( UGCUpdateHandle_t handle, const char *pchKey ) { return false; }
	bool AddItemKeyValueTag( UGCUpdateHandle_t handle, const char *pchKey, const char *pchValue ) { return false; }
	bool AddItemPreviewFile( UGCUpdateHandle_t handle, const char *pszPreviewFile, EItemPreviewType type ) { return false; }
	bool AddItemPreviewVideo( UGCUpdateHandle_t handle, const char *pszVideoID ) { return false; }
	bool UpdateItemPreviewFile( UGCUpdateHandle_t handle, uint32 index, const char *pszPreviewFile ) { return false; }
	bool UpdateItemPreviewVideo( UGCUpdateHandle_t handle, uint32 index, const char *pszVideoID ) { return false; }
	bool RemoveItemPreview( UGCUpdateHandle_t handle, uint32 index ) { return false; }

	CALL_RESULT( SubmitItemUpdateResult_t )
	SteamAPICall_t SubmitItemUpdate( UGCUpdateHandle_t handle, const char *pchChangeNote ) { return k_uAPICallInvalid; }
	EItemUpdateStatus GetItemUpdateProgress( UGCUpdateHandle_t handle, uint64 *punBytesProcessed, uint64* punBytesTotal ) { return k_EItemUpdateStatusInvalid; }

	// Steam Workshop Consumer API
	CALL_RESULT( SetUserItemVoteResult_t )
	SteamAPICall_t SetUserItemVote( PublishedFileId_t nPublishedFileID, bool bVoteUp ) { return k_uAPICallInvalid; }
	CALL_RESULT( GetUserItemVoteResult_t )
	SteamAPICall_t GetUserItemVote( PublishedFileId_t nPublishedFileID ) { return k_uAPICallInvalid; }
	CALL_RESULT( UserFavoriteItemsListChanged_t )
	SteamAPICall_t AddItemToFavorites( AppId_t nAppId, PublishedFileId_t nPublishedFileID ) { return k_uAPICallInvalid; }
	CALL_RESULT( UserFavoriteItemsListChanged_t )
	SteamAPICall_t RemoveItemFromFavorites( AppId_t nAppId, PublishedFileId_t nPublishedFileID ) { return k_uAPICallInvalid; }
	CALL_RESULT( RemoteStorageSubscribePublishedFileResult_t )
	SteamAPICall_t SubscribeItem( PublishedFileId_t nPublishedFileID ) { return k_uAPICallInvalid; }
	CALL_RESULT( RemoteStorageUnsubscribePublishedFileResult_t )
	SteamAPICall_t UnsubscribeItem( PublishedFileId_t nPublishedFileID ) { return k_uAPICallInvalid; }
	uint32 GetNumSubscribedItems() { return 0; }
	uint32 GetSubscribedItems( PublishedFileId_t* pvecPublishedFileID, uint32 cMaxEntries ) { return 0; }

	// get EItemState flags about item on this client
	uint32 GetItemState( PublishedFileId_t nPublishedFileID ) { return 0; }

	// get info about currently installed content on disc for items that have k_EItemStateInstalled set
	// if k_EItemStateLegacyItem is set, pchFolder contains the path to the legacy file itself (not a folder)
	bool GetItemInstallInfo( PublishedFileId_t nPublishedFileID, uint64 *punSizeOnDisk, OUT_STRING_COUNT( cchFolderSize ) char *pchFolder, uint32 cchFolderSize, uint32 *punTimeStamp ) { return false; }

	// get info about pending update for items that have k_EItemStateNeedsUpdate set. punBytesTotal will be valid after download started once
	bool GetItemDownloadInfo( PublishedFileId_t nPublishedFileID, uint64 *punBytesDownloaded, uint64 *punBytesTotal ) { return false; }
		
	// download new or update already installed item. If function returns true, wait for DownloadItemResult_t. If the item is already installed,
	// then files on disk should not be used until callback received. If item is not subscribed to, it will be cached for some time.
	// If bHighPriority is set, any other item download will be suspended and this item downloaded ASAP.
	bool DownloadItem( PublishedFileId_t nPublishedFileID, bool bHighPriority ) { return false; }

	// game servers can set a specific workshop folder before issuing any UGC commands.
	// This is helpful if you want to support multiple game servers running out of the same install folder
	bool BInitWorkshopForGameServer( DepotId_t unWorkshopDepotID, const char *pszFolder ) { return false; }

	// SuspendDownloads( true ) will suspend all workshop downloads until SuspendDownloads( false ) is called or the game ends
	void SuspendDownloads( bool bSuspend ) { return; }

	// usage tracking
	CALL_RESULT( StartPlaytimeTrackingResult_t )
	SteamAPICall_t StartPlaytimeTracking( PublishedFileId_t *pvecPublishedFileID, uint32 unNumPublishedFileIDs ) { return k_uAPICallInvalid; }
	CALL_RESULT( StopPlaytimeTrackingResult_t )
	SteamAPICall_t StopPlaytimeTracking( PublishedFileId_t *pvecPublishedFileID, uint32 unNumPublishedFileIDs ) { return k_uAPICallInvalid; }
	CALL_RESULT( StopPlaytimeTrackingResult_t )
	SteamAPICall_t StopPlaytimeTrackingForAllItems() { return k_uAPICallInvalid; }

	// parent-child relationship or dependency management
	CALL_RESULT( AddUGCDependencyResult_t )
	SteamAPICall_t AddDependency( PublishedFileId_t nParentPublishedFileID, PublishedFileId_t nChildPublishedFileID ) { return k_uAPICallInvalid; }
	CALL_RESULT( RemoveUGCDependencyResult_t )
	SteamAPICall_t RemoveDependency( PublishedFileId_t nParentPublishedFileID, PublishedFileId_t nChildPublishedFileID ) { return k_uAPICallInvalid; }

	// add/remove app dependence/requirements (usually DLC)
	CALL_RESULT( AddAppDependencyResult_t )
	SteamAPICall_t AddAppDependency( PublishedFileId_t nPublishedFileID, AppId_t nAppID ) { return k_uAPICallInvalid; }
	CALL_RESULT( RemoveAppDependencyResult_t )
	SteamAPICall_t RemoveAppDependency( PublishedFileId_t nPublishedFileID, AppId_t nAppID ) { return k_uAPICallInvalid; }
	// request app dependencies. note that whatever callback you register for GetAppDependenciesResult_t may be called multiple times
	// until all app dependencies have been returned
	CALL_RESULT( GetAppDependenciesResult_t )
	SteamAPICall_t GetAppDependencies( PublishedFileId_t nPublishedFileID ) { return k_uAPICallInvalid; }
	
	// delete the item without prompting the user
	CALL_RESULT( DeleteItemResult_t )
	SteamAPICall_t DeleteItem( PublishedFileId_t nPublishedFileID ) { return k_uAPICallInvalid; }
};
