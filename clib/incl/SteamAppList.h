#pragma once
#include "steam/steam_api.h"
#include "steam/isteamapplist.h"

class CSteamAppList : public ISteamAppList
{
public:
	uint32 GetNumInstalledApps() { return 1; }
	uint32 GetInstalledApps( AppId_t *pvecAppID, uint32 unMaxAppIDs ) { return 0; }

	int  GetAppName( AppId_t nAppID, OUT_STRING() char *pchName, int cchNameMax ) { return 0; }
	int  GetAppInstallDir( AppId_t nAppID, char *pchDirectory, int cchNameMax ) { return 0; }

	int GetAppBuildId( AppId_t nAppID ) { return 0; }
};
