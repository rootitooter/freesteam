#pragma once
#include "SteamAppTicket.h"
#include "SteamUser.h"
#include "SteamUtils.h"
#include "SteamApps.h"
#include "SteamUserStats.h"
#include "SteamFriends.h"
#include "SteamRemoteStorage.h"
#include "SteamAppList.h"
#include "SteamUGC.h"
#include "SteamInventory.h"
#include "steam/isteamclient.h"
#include "steam/isteamappticket.h"
#include "steam/steam_api.h"
#include <stdio.h>

class CSteamClient : public ISteamClient
{
public:
    HSteamPipe CreateSteamPipe() {
        printf("[S_API] Called CreateSteamPipe\n");
        return 1;
    }

    bool BReleaseSteamPipe(HSteamPipe pipe) {
        printf("[S_API] Called BReleaseSteamPipe\n");
        return true;
    }

    HSteamUser ConnectToGlobalUser(HSteamPipe pipe) {
        printf("[S_API] Called ConnectToGlobalUser\n");
        return 1;
    }

    HSteamUser CreateLocalUser(HSteamPipe *phSteamPipe, EAccountType eAccT) {
        printf("[S_API] Called CreateLocalUser\n");
        return 1;
    }

    HSteamUser CreateLocalUser(HSteamPipe *phSteamPipe) {
        printf("[S_API] Called CreateLocalUser\n");
        return 0;
    }

    void ReleaseUser(HSteamPipe pipe, HSteamUser user) {
        printf("[S_API] Called ReleaseUser\n");
        return;
    }

    ISteamGameServer *GetISteamGameServer(HSteamUser user, HSteamPipe pipe, const char *pchVersion) {
        printf("[S_API] Called ISteamGameServer %s\n", pchVersion);
        return (ISteamGameServer*)1;
    }
    virtual void SetLocalIPBinding(uint32 IP, uint16 port) {
        // This could be useful to implement later, for LAN support.
        return;
    }

    ISteamUser *GetISteamUser(HSteamUser hSteamUser, HSteamPipe HSteamPipe, const char *pchVersion) {
        // TODO
        printf("[S_API] Called GetISteamUser %s\n", pchVersion);
        return (ISteamUser*)new CSteamUser();
    }

    ISteamFriends *GetISteamFriends(HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char *pchVersion) {
        // TODO
        printf("[S_API] Called GetISteamFriends %s\n", pchVersion);
        return (ISteamFriends*)new CSteamFriends();
    }

    ISteamUtils *GetISteamUtils(HSteamPipe hSteamPipe, const char *pchVersion) {
        // TODO
        printf("[S_API] Called GetISteamUtils %s\n", pchVersion);
        return (ISteamUtils*)new CSteamUtils();
    }

    ISteamMatchmaking *GetISteamMatchmaking( HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamMatchmaking %s\n", pchVersion);
        return (ISteamMatchmaking*)1;
    }

	ISteamMatchmakingServers *GetISteamMatchmakingServers( HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamMatchmakingServers %s\n", pchVersion);
        return (ISteamMatchmakingServers*)1;
    }


	void *GetISteamGenericInterface( HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // We'll want to override this later to bypass DRM!
        printf("Called GetISteamGenericInterface %s\n", pchVersion);

        // Let's just implement DRM for now...
        if (strcmp(pchVersion, STEAMAPPTICKET_INTERFACE_VERSION) == 0) {
            return (ISteamAppTicket*)new CSteamAppTicket();
        }
        fprintf(stderr, "[GetISteamGenericInterface] Couldn't find interface\n");
        return NULL;
    }

	ISteamUserStats *GetISteamUserStats( HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamUserStats %s\n", pchVersion);
        return (ISteamUserStats*)new CSteamUserStats();
    }

	ISteamGameServerStats *GetISteamGameServerStats( HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamGameServerStats %s\n", pchVersion);
        return (ISteamGameServerStats*)1;
    }

	ISteamApps *GetISteamApps( HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamApps %s\n", pchVersion);
        return (ISteamApps*)new CSteamApps();
    }

	ISteamNetworking *GetISteamNetworking( HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamNetworking %s\n", pchVersion);
        return (ISteamNetworking*)1;
    }

	ISteamRemoteStorage *GetISteamRemoteStorage( HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamRemoteStorage %s\n", pchVersion);
        return (ISteamRemoteStorage*)new CSteamRemoteStorage();
    }

	ISteamScreenshots *GetISteamScreenshots( HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamScreenshots %s\n", pchVersion);
        return (ISteamScreenshots*)1;
    }

	uint32 GetIPCCallCount() {
        printf("[S_API] Called GetIPCCallCount\n");
        return 0;
    }

	void SetWarningMessageHook( SteamAPIWarningMessageHook_t pFunction ) {
        printf("[S_API] Called SetWarningMessageHook\n");
        return;
    }

	bool BShutdownIfAllPipesClosed() {
        printf("[S_API] Called BShutdownIfAllPipesClosed\n");
        return 0;
    }

	ISteamHTTP *GetISteamHTTP( HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamHTTP %s\n", pchVersion);
        return (ISteamHTTP*)1;
    }

	ISteamController *GetISteamController( HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamController %s\n", pchVersion);
        return (ISteamController*)1;
    }

	ISteamUGC *GetISteamUGC( HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        printf("[S_API] Called GetISteamUGC %s\n", pchVersion);
        return (ISteamUGC*)new CSteamUGC();
    }

	ISteamAppList *GetISteamAppList( HSteamUser hSteamUser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamAppList %s\n", pchVersion);
        return (ISteamAppList*)new CSteamAppList();
    }

	ISteamMusic *GetISteamMusic( HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamMusic %s\n", pchVersion);
        return (ISteamMusic*)1;
    }

	ISteamMusicRemote *GetISteamMusicRemote(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char *pchVersion) {
        // TODO
        printf("[S_API] Called GetISteamMusicRemote %s\n", pchVersion);
        return (ISteamMusicRemote*)1;
    }

	ISteamHTMLSurface *GetISteamHTMLSurface(HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char *pchVersion) {
        // TODO
        printf("[S_API] Called GetISteamHTMLSurface %s\n", pchVersion);
        return (ISteamHTMLSurface*)1;
    }

    ISteamInventory *GetISteamInventory( HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        printf("[S_API] Called GetISteamInventory %s\n", pchVersion);
        return (ISteamInventory*) new CSteamInventory();
    }

	ISteamVideo *GetISteamVideo( HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamVideo %s\n", pchVersion);
        return (ISteamVideo*)1;
    }

	ISteamParentalSettings *GetISteamParentalSettings( HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char *pchVersion ) {
        // TODO
        printf("[S_API] Called GetISteamParentalSettings %s\n", pchVersion);
        return (ISteamParentalSettings*)1;
    }


    STEAM_PRIVATE_API(void RunFrame() { return; })
	STEAM_PRIVATE_API(void *DEPRECATED_GetISteamUnifiedMessages( HSteamUser hSteamuser, HSteamPipe hSteamPipe, const char *pchVersion ) { return 0; })
    STEAM_PRIVATE_API(void DEPRECATED_Set_SteamAPI_CPostAPIResultInProcess( void (*)() ) { return; })
	STEAM_PRIVATE_API(void DEPRECATED_Remove_SteamAPI_CPostAPIResultInProcess( void (*)() ) { return; })
	STEAM_PRIVATE_API(void Set_SteamAPI_CCheckCallbackRegisteredInProcess( SteamAPI_CheckCallbackRegistered_t func ) { return; })
};
